const emailInput = document.getElementById('email_input');
const invalidMsg = document.getElementById('invalid_toast');
const successMsg = document.getElementById('success_toast');

const form = document.querySelector('.form')

// This function is for checking email correct format
 
function validateEmail(emailValue) {
    let value = /\S+@\S+\.\S+/;
    return value.test(emailValue);
}

form.addEventListener('submit', function (e) {
    e.preventDefault()
    setTimeout(() => form.submit(), 2000)

    if (validateEmail(emailInput.value)) {
        successMsg.classList.add('active')
    } else {
        successMsg.classList.remove('active')
    }
});

emailInput.addEventListener('invalid', function (e) {
    let inputValue = e.target
    console.log(inputValue.validity)
    if (inputValue.validity.typeMissmatch || inputValue.validity.valueMissing) {
        inputValue.setCustomValidity(' ');
        invalidMsg.classList.add('activ')
        successMsg.classList.remove('active')
        this.classList.add('ivalid')
    } else {
        invalidMsg.classList.remove('active')
        successMsg.classList.add('active')
        this.classList.remove('invalid')
    }
});

emailInput.addEventListener('input' , function () {
    invalidMsg.classList.remove('active')
    this.classList.remove('invalid')
});

// disable the enter key of your keyboard, form submit only using But ton

document.addEventListener('keypress' , function (e) {
    if (e.keyCode === 13 || e.which === 13) {
        e.preventDefault()
    }
});